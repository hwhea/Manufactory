﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityPlacement : MonoBehaviour {

    public static EntityPlacement instance = null;

    EntityBase entityInHand = null;
    [SerializeField] GameObject objectInHand = null;
    private float mouseWheelRotationY;
    private float mouseWheelRotationX;
    private Vector3 currentPos;
    private float currentYPos;

    [SerializeField]
    private Material ghostBad;
    [SerializeField]
    private Material ghostGood;
   

    int layerNoToIgnore = 8; //set this
    LayerMask layerToIgnore;

    //snapping
    [SerializeField]  bool entitySnappable = false;
    GameObject mySnapPoint = null;
    GameObject overSnapPoint = null;

    //*** Handling Itemgroups ***
    int entitySelectedScrollIndex = 0;


    private void Awake()
    {
        instance = this;
        layerToIgnore = ~(1 << layerNoToIgnore);
    }

    private void Update () {
        if (objectInHand != null)
        {
            Cursor.visible = false;
            MovePlaceableObject();
            HandleEntitySwitching();
            EntityMaterialCheck();
            HandleEntityTransforms();

            if (Input.GetMouseButtonDown(0))
            {
                if (CanPlace())
                {
                    PlaceObject();
                }
                else
                {

                }
            }
            else if (Input.GetMouseButtonDown(1))
            {
                ReturnToItemSelect();
            }
        }
        else
        {
            Cursor.visible = true;
        }
	}

    private void HandleEntityTransforms()
    {
        if(Input.GetKeyDown(KeyCode.C))
        {
            Debug.Log("Higher!");
            currentYPos = (Mathf.Floor(currentYPos) + 1);
        }
        else if(Input.GetKeyDown(KeyCode.X))
        {
            if(currentYPos > 0)
            {
                currentYPos = (Mathf.Floor(currentYPos) - 1);
            }
            
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            objectInHand.transform.eulerAngles = new Vector3(objectInHand.transform.eulerAngles.x, objectInHand.transform.eulerAngles.y+45, objectInHand.transform.eulerAngles.z);
        }
    }

    private void ReturnToItemSelect()
    {
        DestroyObjectInHand();
        UIManager.instance.ShowLastPanel(true);
    }

    private void PlaceObject()
    {
        for (int i = 0; i < objectInHand.transform.childCount; i++)
        {
            objectInHand.transform.GetChild(i).gameObject.layer = entityInHand.modelPrefab.layer;
        }
        objectInHand.GetComponent<Collider>().enabled = true;
        
        GameObject spawnedGO = ItemSpawner.SpawnItemAtPosition(entityInHand.entityID, currentPos, objectInHand.transform.rotation);

        if (overSnapPoint != null && mySnapPoint != null)
        {
            spawnedGO.GetComponent<SnapManager>().CheckForSnapLock();
        }

        PickupObject(entityInHand.entityID);
    }

    private void HandleEntitySwitching()
    {
        if(Input.mouseScrollDelta.y != 0)
        {
            List<EntityBase> entityGroup = EntityDatabase.FindEntityGroup(entityInHand.entityGroup);

            if (entityGroup.Count > 1)
            {
                if (Input.mouseScrollDelta.y > 0)
                {

                    if (entityGroup.Count - 1 <= entitySelectedScrollIndex)
                        entitySelectedScrollIndex = 0;
                    else
                        entitySelectedScrollIndex += 1;

                }
                else if (Input.mouseScrollDelta.y < 0)
                {
                    if (entitySelectedScrollIndex <= 0)
                        entitySelectedScrollIndex = entityGroup.Count - 1;
                    else
                        entitySelectedScrollIndex -= 1;
                }
            }
            PickupObject(entityGroup[entitySelectedScrollIndex].entityID);
        }
        
        
    }

    private void EntityMaterialCheck()
    {
        bool cp = CanPlace();

        for (int i = 0; i < objectInHand.transform.childCount; i++)
        {
            GameObject child = objectInHand.transform.GetChild(i).gameObject;
            AssignPlaceableMaterial(child, cp);
        }

        AssignPlaceableMaterial(objectInHand, cp);

    }

    private void AssignPlaceableMaterial(GameObject obj, bool cp)
    {
        if (obj.GetComponent<Renderer>() != null)
        {
            Material[] mat = obj.GetComponent<Renderer>().materials;
            for (int i = 0; i < mat.Length; i++)
            {
                mat[i] = (cp ? ghostGood : ghostBad);
            }
            obj.GetComponent<Renderer>().materials = mat;
        }
    }

    private bool CanPlace()
    {
        return !objectInHand.GetComponent<PlacementClipDetector>().isColliding;
    }

    private void MovePlaceableObject()
    {

        
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        
        RaycastHit hitInfo; 

        if(Physics.Raycast(ray, out hitInfo, Mathf.Infinity, layerToIgnore))
        {
            currentPos = hitInfo.point;
            currentPos.y = currentYPos + (objectInHand.GetComponent<Collider>().bounds.extents.y);

            if (entitySnappable)
            {
                if(hitInfo.collider.gameObject.GetComponent<SnapPoint>() != null && hitInfo.collider.gameObject.GetComponent<SnapPoint>().canSnap)
                {
                    //currentPos = hitInfo.collider.gameObject.transform.position;
                    /*
                     * 
                     * Get difference between the object in hand snappoint and ours, apply that to GO
                     * 
                    */

                    //gd

                    float closestChildDist = Mathf.Infinity;
                    int closestChildIdx = 0;

                    for (int i = 0; i < objectInHand.transform.childCount; i++)
                    {
                        GameObject child = objectInHand.transform.GetChild(i).gameObject;

                        float dist = Vector3.Distance(child.transform.position, hitInfo.collider.gameObject.transform.position);

                        if (dist < closestChildDist && child.GetComponent<SnapPoint>() != null)
                        {
                            closestChildDist = dist;
                            closestChildIdx = i;
                        }

                    }

                    overSnapPoint = hitInfo.collider.gameObject;
                    mySnapPoint = objectInHand.transform.GetChild(closestChildIdx).gameObject;

                    Vector3 diff = objectInHand.transform.position - objectInHand.transform.GetChild(closestChildIdx).transform.position;

                    //objectInHand.transform.rotation = hitInfo.collider.gameObject.transform.rotation;
                    currentPos = hitInfo.collider.gameObject.transform.position + diff;
                    //currentPos = hitInfo.transform.position - ((hitInfo.collider.transform.parent.position - hitInfo.collider.gameObject.transform.position) /2);
                }
                else
                {
                    overSnapPoint = null;
                    mySnapPoint = null;
                }
                
            }

            objectInHand.transform.position = currentPos;
            //objectInHand.transform.rotation = Quaternion.FromToRotation(objectInHand.transform.up, hitInfo.normal);

        }
    }

    private void SetAllCollidersTriggers(bool active)
    {
        foreach(Collider c in objectInHand.GetComponents<Collider>())
        {
            c.isTrigger = active;
        }
        foreach(Collider c in objectInHand.GetComponentsInChildren<Collider>())
        {
            c.isTrigger = active;
        }
    }

    private void SetAllRigidbodiesKinematic(bool kinematic)
    {
        foreach (Rigidbody r in objectInHand.GetComponents<Rigidbody>())
        {
            r.isKinematic = kinematic;
        }
        foreach (Rigidbody r in objectInHand.GetComponentsInChildren<Rigidbody>())
        {
            r.isKinematic = kinematic;
        }
    }

    public void PickupObject(string pickedUpEntityID)
    {
        DestroyObjectInHand();
        entityInHand = EntityDatabase.FindItem(pickedUpEntityID);
        objectInHand = ItemSpawner.SpawnGhostAtPosition(entityInHand.entityID, Vector3.zero);
        objectInHand.AddComponent<PlacementClipDetector>();
        SetAllCollidersTriggers(true);
        SetAllRigidbodiesKinematic(true);
        objectInHand.layer = layerNoToIgnore;

        for (int i = 0; i < objectInHand.transform.childCount; i++)
        {
            objectInHand.transform.GetChild(i).gameObject.layer = layerNoToIgnore;
        }

        UIManager.instance.ShowLastPanel(false);

        entitySnappable = (objectInHand.GetComponentInChildren<SnapPoint>()) ;

    }

    public void DestroyObjectInHand()
    {
        if(objectInHand != null || entityInHand != null)
        {
            Destroy(objectInHand);
            entityInHand = null;
        }
        
                
    }
}
