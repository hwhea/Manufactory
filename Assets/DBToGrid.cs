﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DBToGrid : MonoBehaviour {


    private GameObject gridParent;
    [SerializeField] private GameObject GridSquareTemplate;
    [SerializeField] private EntityBase.EntityType filter;

    void Start () {
        gridParent = this.gameObject;

        List<EntityBase> genList = new List<EntityBase>();
        foreach(List<EntityBase> entityGroup in EntityDatabase.GetAllGroupsOfType(filter))
        {
            genList.Add(entityGroup[0]);
        }

        GenerateGridFromList(genList);
    }

    void GenerateGridFromList(List<EntityBase> list)
    {
        foreach (EntityBase dbitem in list)
        {
            AddGridItem(dbitem);
        }
    }

    void AddGridItem(EntityBase gridItem)
    {
        GameObject newItemParent = (GameObject)GameObject.Instantiate(GridSquareTemplate, gridParent.transform);

        if (newItemParent.GetComponent<GridItemBase>() == null && newItemParent.GetComponent<GridItemPlaceable>() == null)
        {
            newItemParent.AddComponent<GridItemBase>();

        }
        newItemParent.GetComponent<GridItemBase>().Initialize(gridItem);
    }

}
