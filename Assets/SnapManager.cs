﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapManager : MonoBehaviour {

    SnapPoint[] mySnapPoints = new SnapPoint[] { };

	void Start () {
        mySnapPoints = GetComponentsInChildren<SnapPoint>();
	}

    public void CheckForSnapLock()
    {
        mySnapPoints = GetComponentsInChildren<SnapPoint>();
        for (int i = 0; i < mySnapPoints.Length; i++)
        {
            mySnapPoints[i].CheckIfSnapped();
        }
        
    }

    void Update () {
		
	}
}
