﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapPoint : MonoBehaviour {

    //https://forum.unity.com/threads/snap-gameobjects-at-runtime.244421/

    public GameObject snappedTo;
    public bool canSnap = true;

    public float dbg_randID = 77;

    void Awake () {
        dbg_randID = Random.Range(0, 1000);

    }
	
	void Update () {
		
	}

    private void HandleCanSnap()
    {
        canSnap = snappedTo == null;

        transform.GetChild(0).gameObject.SetActive(canSnap);
       
    }

    public void CheckIfSnapped()
    {
        //todo
    }

    public void SnapPointToPoint(SnapPoint fromPoint)
    {
        snappedTo = fromPoint.gameObject;
        HandleCanSnap();
    }

    public void UnSnap()
    {
        snappedTo.GetComponent<SnapPoint>().UnSnap();
        snappedTo = null;
        HandleCanSnap();
    }

    void OnTriggerEnter(Collider other)
    {
    }

}
