﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(EntityDatabase))]
public class GameDatabaseInspector : Editor {

    public static readonly GUIStyle splitter;
    string searchTerm = "";
    Dictionary<string, bool> showList = new Dictionary<string, bool>();
    
    static GameDatabaseInspector()
    {
    

    }

    public override void OnInspectorGUI()
    {

        searchTerm = EditorGUILayout.TextField("Search Database: ", searchTerm);

        GUILayout.Label("DB item count: " + EntityDatabase.items.Count);

        
        foreach (EntityBase myitem in EntityDatabase.GetAllEntitiesUngrouped())
        {
            if (myitem.entityName.ToLower().Contains(searchTerm.ToLower()) || myitem.entityID.ToLower().Contains(searchTerm.ToLower()))
            {

                if (!showList.ContainsKey(myitem.entityID))
                {
                    showList.Add(myitem.entityID, false);
                }
                else
                {
                    Debug.Log("Key for " + myitem.entityID);
                    showList[myitem.entityID] = EditorGUILayout.Foldout(showList[myitem.entityID], myitem.entityID);
                }
                

                if (showList[myitem.entityID])
                {
                    GUILayout.BeginHorizontal(EditorStyles.helpBox);
                    GUILayout.Label("Item Name: " + myitem.entityName);
                    GUILayout.Label("Price: " + myitem.marketPrice.ToString());
                    GUILayout.EndHorizontal();
                    GUILayout.Label("Item GO: " + myitem.modelPrefab.name);
                }
            }
            
        }
        
    }

    
}
#endif