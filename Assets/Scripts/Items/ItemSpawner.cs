﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawner : MonoBehaviour {

	public static GameObject SpawnItemAtPosition(string item, Vector3 position, Quaternion rotation)
    {
        GameObject itemtr = BuildItemFromConfig(item);
        itemtr.transform.rotation = rotation;
        itemtr.transform.position = position;
        return itemtr;
    }

    public static GameObject SpawnItemAtPosition(string item, GameObject customModel, Vector3 position, Quaternion rotation)
    {
        GameObject itemtr = BuildItemFromConfig(item, customModel);
        itemtr.transform.rotation = rotation;
        itemtr.transform.position = position;
        return itemtr;
    }
	
    public static GameObject SpawnGhostAtPosition(string item, Vector3 position)
    {
        GameObject go = BuildItemFromConfig(item);
        go.transform.position = position;
        return go;
    }

    public static void WriteGhostSnapPointsToItem(GameObject ghost, GameObject target)
    {
        for (int i = 0; i < target.transform.childCount; i++)
        {
            if(target.transform.GetChild(i).GetComponent<SnapPoint>() != null)
                Destroy(target.transform.GetChild(i).gameObject);
        }

        for (int i = 0; i < ghost.transform.childCount; i++)
        {
            if (ghost.transform.GetChild(i).GetComponent<SnapPoint>() != null)
                ghost.transform.GetChild(i).SetParent(target.transform);
        }
    }

    //returns a go from the imported configs.
    public static GameObject BuildItemFromConfig(string itemID, GameObject go = null)
    {
        EntityBase newItem = EntityDatabase.FindItem(itemID);

        if (newItem == null)
        {
            Debug.LogError("Couldn't find item " + itemID + "; couldn't spawn.");
            return null;
        }

        GameObject newItemGO = null;

        if (go == null)
        {
            newItemGO = Instantiate(newItem.modelPrefab);
        }
        else
        {
            newItemGO = Instantiate(go);
        }

        Debug.Log("Placed " + newItemGO.name);

        ItemInfo ii = newItemGO.AddComponent<ItemInfo>();
        ii.SetItemInfo(newItem.entityID);

        SnapManager sm = newItemGO.AddComponent<SnapManager>();

        /*
         *  Here we add handlers (is the object heatable etc) 
         * 
        */

        return newItemGO;
    }

}
