﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlacementClipDetector : MonoBehaviour {


    public bool isColliding = false;

    Collider collider;

	void Start () {
        collider = GetComponent<Collider>();
	}
	
	void Update () {
		
	}

    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.GetComponent<SnapPoint>() == null)
        {
            isColliding = true;
            Debug.Log("COLLISION WITH: " + col.gameObject.name);
        }
            

        
    }

    void OnTriggerExit(Collider col)
    {
        isColliding = false;
    }
}
