﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EntityDatabase : MonoBehaviour {

	public static List<List<EntityBase>> items = new List<List<EntityBase>>();

    static void Start () {
		PopulateDB ();
	}

    static void PopulateDB(){
		
	}

    public static void AddEntity(EntityBase item)
    {
        //Find if there is already a list of entities in this group. 
        List<EntityBase> entityGroup = FindEntityGroup(item.entityGroup);

        //if not, just create one.
        if(entityGroup == null)
        {
            entityGroup = new List<EntityBase>();
            entityGroup.Add(item);
            items.Add(entityGroup);
        }
        else
        {
            entityGroup.Add(item);
        }
        
    }

    internal static List<EntityBase> GetAllEntitiesOfType(EntityBase.EntityType filter)
    {

        List<EntityBase> retList = new List<EntityBase>();

        foreach (EntityBase eb in GetAllEntitiesUngrouped()){
            if(eb.entityType == filter)
            {
                retList.Add(eb);
            }
        }

        return retList;
    }

    internal static List<List<EntityBase>> GetAllGroupsOfType(EntityBase.EntityType filter)
    {

        List<List<EntityBase>> retList = new List<List<EntityBase>>();

        foreach (List<EntityBase> eb in GetAllEntityGroups())
        {
            if (eb[0].entityType == filter)
            {
                retList.Add(eb);
            }
        }

        return retList;
    }

    public static List<EntityBase> GetAllEntitiesUngrouped()
    {
        List<EntityBase> retlist = new List<EntityBase>();

        foreach(List<EntityBase> leb in items)
        {
            foreach(EntityBase eb in leb)
            {
                retlist.Add(eb);
            }
        }

        return retlist;
    }

	/*
	void AddItem(int itemID, string itemName,  GameObject prefab){
		if (!ItemExists (itemName)) {
			items.Add (new DBItem (itemID, itemName, prefab));
		} else {
			Debug.LogError ("Error adding item " + itemName + " to DB. That name already exists.");
		}
	}

	void AddItem(int itemID, string itemName, GameObject prefab, string desc){
		if (!ItemExists (itemName)) {
			items.Add (new DBItem (itemID, itemName, prefab, desc));
		} else {
			Debug.LogError ("Error adding item " + itemName + " to DB. That name already exists.");
		}
	}
	*/

	public static EntityBase FindItem(string id){
        foreach (List<EntityBase> itemGroup in items)
        {
            foreach(EntityBase item in itemGroup)
            {
                if (item.entityID == id)
                {
                    return item;
                }
            }
        }
		return null;
	}

    public static List<EntityBase> FindEntityGroup(string groupName)
    {
        foreach(List<EntityBase> eg in GetAllEntityGroups())
        {
            if(eg[0].entityGroup == groupName)
            {
                return eg;
            }
        }
        return null;
    }

    public static List<List<EntityBase>> GetAllEntityGroups()
    {
        return items;
    }

    public static List<EntityBase> GetAllPrimaryEntities()
    {
        List<EntityBase> retList = new List<EntityBase>();

        foreach (List<EntityBase> itemGroup in items)
        {
            retList.Add(itemGroup[0]);
        }

        return retList;
    }
		

	public static bool EntityExists(string id){
		return FindItem(id) != null;
	}




}
