﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemInfo : MonoBehaviour {

    [SerializeField]
    public string itemID;
    
    public void SetItemInfo(string itemID)
    {
        this.itemID = itemID;
    }

}
