﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class AddonImporter : MonoBehaviour {

    //Part One: Iterate over file system, find all .mfdata files. 
    //Part Two: Read these files one by one and put them where they need to be.

    string gameDataPath = "GameData";
    INIParser ini = new INIParser();
    UnityEngine.Object[] allAssets = null;

    [SerializeField]
    private GameObject snapPointVisual;

    void Start () {

        allAssets = LoadAllAssets();
        if(allAssets == null)
        {
            Debug.LogError("Assets not loaded.");
            return;
        }

        ParseMfdataFiles(FindMfdataFiles(allAssets));
    }

    private UnityEngine.Object[] LoadAllAssets()
    {
        string[] assetBundleList = GetAllFilesWithExtension(".mfaddon");
        Debug.Log(assetBundleList.Length);
        List<UnityEngine.Object> objectList = new List<UnityEngine.Object>();
        foreach (string assetBundlePath in assetBundleList)
        {
            AssetBundle loadedBundle = AssetBundle.LoadFromFile(assetBundlePath);
            if(loadedBundle == null)
            {
                Debug.LogError("Failed to load asset bundle at: " + assetBundlePath);
                break;
            }
            else
            {

                UnityEngine.Object[] objectsInAB = loadedBundle.LoadAllAssets();
                for (int i = 0; i < objectsInAB.Length; i++)
                {
                    Debug.Log("Loaded asset: " + objectsInAB[i].name);
                    objectList.Add(objectsInAB[i]);
                }
            }
        }

        return objectList.ToArray();
    }

    private string[] GetAllFilesWithExtension(string ext)
    {
        string path = Application.dataPath + "/" + gameDataPath + "/";
        Debug.Log("Searching " + path + " for " + ext + " files...");
        return Directory.GetFiles(path, "*"+ext, SearchOption.AllDirectories);
    }

    private UnityEngine.Object[] FindMfdataFiles(UnityEngine.Object[] objects)
    {
        List<UnityEngine.Object> mfdataFiles = new List<UnityEngine.Object>();
        for (int i = 0; i < objects.Length; i++)
        {
            if (objects[i].name.EndsWith(".mfdata"))
            {
                mfdataFiles.Add(objects[i]);
            }
        }
        return mfdataFiles.ToArray();
    }

    private void ParseMfdataFiles(UnityEngine.Object[] objects)
    {
        for (int i = 0; i < objects.Length; i++)
        {
            TextAsset file = (TextAsset)objects[i];
            ini.OpenFromString(file.text);

            string entityID = ini.ReadValue("Info", "entityID", "ERROR");
            string entityName = ini.ReadValue("Info", "entityName", "ERROR");
            string entityDesc = ini.ReadValue("Info", "entityDesc", "ERROR");

            if (entityID.Equals("ERROR") || entityName.Equals("ERROR") || entityDesc.Equals("ERROR"))
            {
                Debug.LogError("entity doesn't contain all relevant information: " + entityID);
                break;
            }

            if (EntityDatabase.EntityExists(entityID))
            {
                Debug.LogError("entity with this ID already exists.");
                break;
            }

            EntityBase entityToAdd = null;

            if (ini.IsSectionExists("Entity"))
            {
                entityToAdd = HandleEntityFile(file);
            }
            else
            {
                Debug.LogWarning("Found MFDATA file with no entity section: " + objects[i].name);
                break;
            }

            if(entityToAdd != null)
            {
                if (ini.IsSectionExists("Machine"))
                {
                    HandleMachineEntity(entityToAdd);
                }
            }
            
            ini.Close();
        }
    }

    /*
     *  HANDLERS FOR DIFFERENT OBJECTS
     * */

    private EntityBase HandleEntityFile(TextAsset config)
    {
        ini.OpenFromString(config.text);

        string entityID = ini.ReadValue("Info", "entityID", "ERROR");
        string entityName = ini.ReadValue("Info", "entityName", "ERROR");
        string entityDesc = ini.ReadValue("Info", "entityDesc", "ERROR");
        string entityGroup = ini.ReadValue("Group", "groupName", "NONE");
        EntityBase.EntityType entityType;


        try
        {
            entityType = (EntityBase.EntityType)Enum.Parse(typeof(EntityBase.EntityType), ini.ReadValue("Info", "entityType", "ERROR"), true);
        }
        catch (Exception e)
        {
            Debug.LogError("Entity with ID " + entityID + " has unknown type.");
            return null;
        }


        float entityPrice = -1;

        if(entityDesc.Length > 150)
        {
            entityDesc = "This entity's description was too long. Tell the creator so they can get it fixed.";
        }

        try
        {
            entityPrice = float.Parse(ini.ReadValue("Entity", "price", "ERROR"));

        }
        catch (System.FormatException e)
        {
            Debug.LogError("entity doesn't contain valid price.");
            return null;
        }

        if(entityPrice < 0)
        {
            Debug.LogError("Price is < 0");
            return null;
        }

        if(entityGroup == "NONE")
        {
            entityGroup = entityID;
        }

        UnityEngine.Object entityObject = null;
        Sprite entityThumbnail = null;

        for (int i = 0; i < allAssets.Length; i++)
        {
            if(allAssets[i].name.ToLower() == entityID)
            {
                //This is the asset we're looking for.
                Debug.Log("Found " + entityID);
                entityObject = allAssets[i];
            }
            else if(allAssets[i].name.ToLower() == entityID + "_thumb")
            {
                Texture2D objAsTex = allAssets[i] as Texture2D;
                Sprite newSprite = Sprite.Create(objAsTex, new Rect(0f, 0f, objAsTex.width, objAsTex.height), Vector2.zero);
                entityThumbnail = newSprite;
            }
        }

        if(entityObject == null)
        {
            Debug.LogError("The gameobject associated with "+entityID+" could not be found");
            return null;
        }

        if(entityThumbnail == null)
        {
            Debug.LogError("The thumbnail associated with " + entityID + " could not be found");
            return null;
        }

        EntityBase newentity = new EntityBase(entityType, entityID, entityName, entityPrice, (GameObject)entityObject, entityDesc, entityThumbnail, entityGroup);

        EntityDatabase.AddEntity(newentity);
        Debug.Log("Added "+entityID+" to database.");

        return newentity;
    }

    private void HandleMachineEntity(EntityBase obj)
    {
        for (int i = 0; i < obj.modelPrefab.transform.childCount; i++)
        {
            GameObject child = obj.modelPrefab.transform.GetChild(i).gameObject;

            HandleSnapPoints(child);
        }
    }

    /*
     *  Script additions. 
    */

    private void HandleSnapPoints(GameObject obj)
    {
        if(obj.name == "snappoint")
        {
            obj.AddComponent<SnapPoint>();
            SphereCollider sc = obj.AddComponent<SphereCollider>();
            Instantiate(snapPointVisual, obj.transform.position, Quaternion.identity, obj.transform);
            sc.radius = 0.5f;
        }
    }
}
