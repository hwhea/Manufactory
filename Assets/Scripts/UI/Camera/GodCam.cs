﻿using UnityEngine;
using System.Collections;

public class GodCam : MonoBehaviour {

    //Orbit Cam
    bool orbitCamEnabled = false;
    float horizontalMove = 2f;
    float verticalMove = 2f;
    Vector3 newTarget;

    //Translate Cam
	float movementSpeed = 10f;
	float zoomSpeed = 50f;
	float minYPosition = 0;
	float lookSensitivity = 2;
	float smoothing = 0.5f;


	Vector2 targetRotVector;
	Quaternion targetRotation;
	Vector3 targetPosition;

	void Start () {
		Vector3 offset = new Vector3 (10, 10);
		targetRotVector = transform.rotation.eulerAngles;

        
	}

	void Update () {
		transform.Translate (Vector3.right * Input.GetAxis ("Horizontal") * movementSpeed * Time.deltaTime * (transform.position.y /2));
		
		transform.Translate ((Vector3.forward) * Input.GetAxis ("Vertical") * movementSpeed * Time.deltaTime * (transform.position.y /2));
		

        if (orbitCamEnabled)
        {
            newTarget = new Vector3(transform.position.x, 0, transform.position.z - 3);
            //targetPosition = newTarget;
            transform.LookAt(targetPosition);
        }

        //Handles rotation
        if (Input.GetMouseButton(2)) {
            if (orbitCamEnabled)
            {
                MoveHorizontal(Input.GetAxis("Mouse X"));
                MoveVertical(Input.GetAxis("Mouse Y"));
            }
            else
            {
                Cursor.visible = false;
                targetRotVector.y += Input.GetAxis("Mouse X") * lookSensitivity;
                targetRotVector.x -= Input.GetAxis("Mouse Y") * lookSensitivity;
                targetRotation = Quaternion.Euler(targetRotVector.x, targetRotVector.y, 0.0f);
            }
		} else {
			Cursor.visible = true;
		}

		transform.rotation = Quaternion.Lerp( transform.rotation, targetRotation, ( 1.0f  - smoothing) );
	}

    void MoveHorizontal(float delta)
    {
        transform.RotateAround(targetPosition, Vector3.up, horizontalMove * delta);
    }

    void MoveVertical(float delta)
    {
        transform.RotateAround(targetPosition, transform.TransformDirection(Vector3.right), verticalMove * delta);
    }
}
