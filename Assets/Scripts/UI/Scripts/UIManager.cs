﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIManager : MonoBehaviour {

    public static UIManager instance;
    List<GameObject> registeredPanels = new List<GameObject>();
    GameObject lastPanel = null;

	void Start () {
		instance = this;
	}
    
    public void ShowLastPanel(bool active)
    {
        lastPanel.SetActive(active);
    }

    public void TogglePanel(GameObject panel)
    {
        if (!registeredPanels.Contains(panel))
            registeredPanels.Add(panel);

        foreach(GameObject p in registeredPanels)
        {
            if (p == panel)
            {
                lastPanel = p;
                p.SetActive(!p.activeSelf);
            }else
            {
                p.SetActive(false);
            }
            
        }

        
    }


	public void ToggleBuildPanel(){
		GameObject buildPanel = transform.Find ("BuildPanel").gameObject;
		buildPanel.SetActive (!buildPanel.activeSelf);
	}
}
