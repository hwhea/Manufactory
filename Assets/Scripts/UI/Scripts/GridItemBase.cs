﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GridItemBase : MonoBehaviour, IPointerClickHandler
{ 
    protected EntityBase entity;

    public void Initialize(EntityBase gridItem)
    {
        entity = gridItem;
        GameObject newItem = (GameObject)GameObject.Instantiate(new GameObject(), this.transform);
        newItem.AddComponent<Image>();
        newItem.GetComponent<Image>().sprite = entity.thumbnail;
        newItem.GetComponent<RectTransform>().sizeDelta = new Vector2(70, 70);
    }

	void Start () {
		
	}
	
	void Update () { 
       
    }

    public virtual void HandleClick()
    {
        Debug.Log("Override me!");
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        HandleClick();
    }
}
