﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridItemPlaceable : GridItemBase {

    public override void HandleClick()
    {
        EntityPlacement.instance.PickupObject(this.entity.entityID);
    }

}
