﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConveyorBelt : MonoBehaviour {

    [SerializeField] bool isInverted = false;
	public bool isPoweredOn = true;
	[SerializeField]float conveyorSpeed = 1f;
	Vector3 conveyorDirection;
	GameObject dragZone;


	void Start () {
       

	}

	void FixedUpdate(){
		if (true) {
            conveyorDirection = (isInverted ? -transform.right : transform.right);
            GetComponent<Rigidbody> ().position -= conveyorDirection * conveyorSpeed * Time.deltaTime;
			GetComponent<Rigidbody> ().MovePosition (GetComponent<Rigidbody> ().position + conveyorDirection * conveyorSpeed * Time.deltaTime);
		}
	}

}
