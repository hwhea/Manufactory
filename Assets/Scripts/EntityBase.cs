﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EntityBase{

    public enum EntityType
    {
        Item,
        Machine
    }

	[SerializeField] public string entityID;
	[SerializeField] public string entityName;
	[SerializeField] public string entityDesc;
	[SerializeField] public float marketPrice;
    [SerializeField] public Sprite thumbnail;
    [SerializeField] public string entityGroup;
    [SerializeField] public EntityType entityType;
    public GameObject modelPrefab;

	public EntityBase(){}

	public EntityBase(EntityType entityType, string entityID, string entityName, float marketPrice, GameObject modelPrefab, string desc, Sprite thumbnail, string entityGroup){
		this.entityDesc = desc;
		this.entityID = entityID;
		this.entityName = entityName;
		this.marketPrice = marketPrice;
		this.modelPrefab = modelPrefab;
        this.thumbnail = thumbnail;
        this.entityGroup = entityGroup;
        this.entityType = entityType;
	}

}
