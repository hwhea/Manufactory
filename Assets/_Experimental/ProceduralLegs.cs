﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProceduralLegs : MonoBehaviour {

    //the main collider, the one we will add legs to 
    Collider col;

    //temp for testing, so we can delete and respawn]
    GameObject[] legs = new GameObject[4];
    GameObject[] legsContainers = new GameObject[4];

    [SerializeField]
    GameObject legObject;
    //

    Vector3 lastPos = Vector3.zero;

    // Use this for initialization
    void Start () {

        //get the largest collider
        col = null;
        foreach (Collider collider in GetComponents<Collider>())
        {
            if(col == null || GetArea(collider.bounds.extents) > GetArea(col.bounds.extents))
            {
                col = collider;
            }
        }

	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            RedrawLegs();
        }
        if (Input.GetMouseButton(1))
        {
            for (int i = 0; i < legs.Length; i++)
            {
                Destroy(legs[i]);
            }
        }

         

        if(lastPos != transform.position)
        {
            for (int i = 0; i < legs.Length; i++)
            {
                Destroy(legs[i]);
                
            }

            for (int i = 0; i < legsContainers.Length; i++)
            {
                Destroy(legsContainers[i]);
            }

            RedrawLegs();
            lastPos = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        }

	}

    void RedrawLegs()
    {
        Vector3[] corners = GetCorners(col);

        for (int i = 0; i < corners.Length; i++)
        {
            legsContainers[i] = (GameObject)Instantiate(new GameObject(), corners[i], Quaternion.identity, col.gameObject.transform);
            legsContainers[i].name = "pasta";
        }

        for (int i = 0; i < legs.Length; i++)
        {
            legs[i] = (GameObject)Instantiate(legObject, corners[i], Quaternion.identity, legsContainers[i].gameObject.transform);
            legs[i].name = "Asda";
            RescaleLegs(legs[i], transform.position.y - (col.bounds.extents.y * 4));
        }
    }

    void RescaleLegs(GameObject ogLeg, float yHeight)
    {
        Vector3 newScale = ogLeg.transform.localScale;
        newScale.y = yHeight;
        ogLeg.transform.localScale = newScale;
    }

    Vector3[] GetCorners(Collider cornersOf)
    {
        Vector3[] corners = new Vector3[4];

        Vector3 center = cornersOf.bounds.center;

        center.y = (cornersOf.gameObject.transform.position.y /2) - (cornersOf.bounds.extents.y*2);
        center.y -= cornersOf.bounds.extents.y;

        corners[0] = new Vector3(center.x - (cornersOf.bounds.extents.x * 0.8f), center.y, center.z - (cornersOf.bounds.extents.z * 0.95f));
        corners[1] = new Vector3(center.x + (cornersOf.bounds.extents.x * 0.8f), center.y, center.z - (cornersOf.bounds.extents.z * 0.95f));
        corners[2] = new Vector3(center.x - (cornersOf.bounds.extents.x * 0.8f), center.y, center.z + (cornersOf.bounds.extents.z * 0.95f));
        corners[3] = new Vector3(center.x + (cornersOf.bounds.extents.x * 0.8f), center.y, center.z + (cornersOf.bounds.extents.z * 0.95f));

        return corners;
    }

    float GetArea(Vector3 extents)
    {
        return (extents.x * extents.y * extents.z);
    }
}
